const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();
const del = require('del');

const jsFiles = [
	'./src/js/all-scripts.js'
];

function styles() {
	return gulp.src('./src/sass/main.scss')
				.pipe(sass().on('error', sass.logError))
				.pipe(autoprefixer({
		            browsers: ['last 2 versions'],
		            cascade: false
		        }))
		        .pipe(cleanCSS({
		        	level: 2
		        }))
				.pipe(gulp.dest('./build/css'))
				.pipe(browserSync.stream());
}

function scripts() {
	return gulp.src(jsFiles)
				.pipe(concat('scripts.js'))
				.pipe(uglify({
					toplevel: true
				}))
				.pipe(gulp.dest('./build/js'))
				.pipe(browserSync.stream());
}

function watch() {
	browserSync.init({
        server: {
            baseDir: "./"
        },
        //tunnle: true
    });

	gulp.watch('./src/sass/**/*.scss', styles);
	gulp.watch('./src/js/**/*.js', scripts);
	gulp.watch('./*.html', browserSync.reload);
}


function clean() {
 	return del(['build/*']);
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
gulp.task('clean', clean);

gulp.task('build', gulp.series(clean,
						gulp.parallel(styles, scripts)
					));

gulp.task('dev', gulp.series('build', 'watch'));